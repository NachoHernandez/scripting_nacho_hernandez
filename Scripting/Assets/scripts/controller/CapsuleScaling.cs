using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour
{
    [SerializeField]
    private Vector3 axes;
    public float ScaleUnits;
    // Update is called once per frame
    void Update()
    {
        axes = Movement_texto.ClampVector3(axes);
        transform.localScale += axes * (ScaleUnits * Time.deltaTime);
    }
}
